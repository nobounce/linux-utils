PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin

FILES = boot-iso regrub
GENTOO_FILES = gen-initram gen-kernel gen-mount-vfs gen-open-crypt repopath

install:
	mkdir -pv $(BINDIR)
	@for file in $(FILES); do install -m 755 $$file $(BINDIR)/$$file && printf "install -m 755 $$file $(BINDIR)/$$file\n"; done

gentoo-install: install
	@for file in $(GENTOO_FILES); do install -m 755 $$file $(BINDIR)/$$file && printf "install -m 755 $$file $(BINDIR)/$$file\n"; done

uninstall:
	@for file in $(FILES) $(GENTOO_FILES); do rm -fv $(BINDIR)/$$file && printf "rm -fv $(BINDIR)/$$file\n"; done

